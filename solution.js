const cheerio = require('cheerio')
const promise = require('request-promise')
const bb = require('bluebird')
const fs = require('fs')

bb.promisifyAll(require("request"));

const base_url = 'https://www.bankmega.com/promolainnya.php'

let allPromosByCat = {}
let categories = []

const getPromo = async(subcat = 1, page = 1) => {
    url = `${base_url}?subcat=${subcat}&page=${page}`
    console.log(`request to url ${url}`);
    
    const options = {
        uri : url,
        transform : function(body){
            return cheerio.load(body)
        } 
    }

    try {
        const $ = await promise(options)
        const CountCat = $('#subcatpromo').find('img').length
        if (subcat === 1 && page === 1) {
            $('#subcatpromo').find('img').each((idxPromo, img) => {
                categories.push(img.attribs.id);
                allPromosByCat[img.attribs.id] =[];
            })
        }
        const imagePromo = $('#imgClass')
        if (imagePromo.length > 0) {
            const arrDetailPromise = []
            imagePromo.each((i, img) => {
                let optionsDetail = {
                    uri : `https://www.bankmega.com/${img.parent.attribs.href}`,
                    transform : function(body){
                        return cheerio.load(body)
                    }
                }
                arrDetailPromise.push(promise(optionsDetail))
            })

            await Promise.all(arrDetailPromise)
            .then(details => {
                details.map(detail =>{
                    const title = detail(".titleinside h3").text()
                    const imgUrl = 'https://www.bankmega.com'+detail(".keteranganinside img").attr("src")
                    const area = detail(".area b").text()
                    const periode = detail(".periode b").eq(0).text() + detail(".periode b").eq(1).text()
                    allPromosByCat[categories[subcat-1]].push({
                        title,
                        imgUrl,
                        area,
                        periode
                    })
                })
            })
            .catch(errFetchDetail => {
                console.log(errFetchDetail);  
            })
            page++
            return getPromo(subcat, page)
        }
        else if (subcat < CountCat) {
            console.log('Next Category');
            subcat++;
            return getPromo(subcat, 1)
        } 
        else{
            const filename = 'solution.json'
            console.log(allPromosByCat);
            const save = JSON.stringify(allPromosByCat, null, 2)
            fs.writeFile(filename, save, 'utf-8', () => {
                console.log('Data disimpan dengan nama '+filename);
            })
            return 'Done'
        }
    }
    catch (error){
        return error
    }
}

getPromo()
.then(promos => {
    console.log(promos);
})
.catch(err => {
    console.log(err);
})